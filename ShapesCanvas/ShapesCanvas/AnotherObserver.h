#ifndef AnotherObserver_H
#define AnotherObserver_H

#include "Observer.h"
#include "ShapeData.h"
#include "LeakedClass.h"
#include <qdebug.h>

class AnotherObserver : public Observer
{
public:
    AnotherObserver() { qDebug() << ("AnotherObserver::constructor"); myResource = new LeakedClass(); }
    ~AnotherObserver() { qDebug() << ("AnotherObserver::destructor"); delete myResource; }

    virtual void update(ShapeData * shapeData)
    {
        qDebug() << ("I have a resource, yay. But it will never die :(.");
    }

private:
    LeakedClass * myResource;
};

#endif // AnotherObserver_H