#pragma once
#include "ShapeGenerator.h"

class FluffyShapeGenerator :
    public ShapeGenerator
{
public:
    FluffyShapeGenerator();
    ~FluffyShapeGenerator();

    virtual QGraphicsItem * makeSquare();
    virtual QGraphicsItem * makeCircle();
};

