
#ifndef Observer_H
#define Observer_H

class ShapeData;
#include <qdebug.h>

class Observer
{
public:
    Observer() { qDebug() << "Observer::constructor"; }
    virtual ~Observer() { qDebug() << "Observer::destructor"; }
    virtual void update(ShapeData * shapeData) = 0;
};

#endif // Observer_H