#include "MainWindow.h"

#include <QGraphicsView>
#include <QGraphicsItem>
#include <QHBoxLayout>

#include <QTimer.h>
#include <qfile.h>
#include <qtextstream.h>

#include "ShapeView.h"
#include "ShapeData.h"
#include "Scene.h"

#include "FluffySquareItem.h"
#include "FluffyCircleItem.h"
#include "GrumpySquareItem.h"
#include "GrumpyCircleItem.h"

#include "ShapeGenerator.h"
#include "FluffyShapeGenerator.h"
#include "GrumpyShapeGenerator.h"

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
{
    ui.setupUi(this);

    connect(ui.myAddItemButton, SIGNAL(clicked()), this, SLOT(addItem()));
    connect(ui.myFaceItemButton, SIGNAL(clicked()), this, SLOT(addFaceItem()));
    connect(ui.mSaveItemsStatesButton, SIGNAL(clicked()), this, SLOT(slotSaveItemsStates()));

    createScene();
    populateSceneWithItems();
    myDefaultShapeData = new ShapeData();
    myShapeGenerators.append(new FluffyShapeGenerator());
    myShapeGenerators.append(new GrumpyShapeGenerator());
}

void MainWindow::addItem()
{
    ShapeData shapeData;
    shapeData.addObserver(this);
    shapeData.setBoundingBox(QRectF(0, 0, 100, 100));
    shapeData.setColor(Qt::green);
    shapeData.setSelectedColor(Qt::red);
    ShapeView * item = new ShapeView();
    item->setShapeData(shapeData);
    scene->addItem(item);
}

void MainWindow::addFaceItem()
{
    int itemStyle = ui.myShapeStyleComboBox->currentIndex();
    int itemShape = ui.myShapeTypeComboBox->currentIndex();

    const int SquareShape = 0;
    const int CircleShape = 1;

    QGraphicsItem * item;
    ShapeGenerator * sg = getTheRightGenerator();

    if (itemShape == SquareShape)
    {
        item = sg->makeSquare();
    }
    else if (itemShape == CircleShape)
    {
        item = sg->makeCircle();
    }

    item->setFlag(QGraphicsItem::ItemIsMovable);
    scene->addItem(item);

}

ShapeGenerator * MainWindow::getTheRightGenerator() const
{
    int itemStyle = ui.myShapeStyleComboBox->currentIndex();
    return myShapeGenerators.at(itemStyle);
}


MainWindow::~MainWindow()
{
    delete scene;
    delete myDefaultShapeData;
}

void MainWindow::createScene()
{
    scene = new Scene(this);
    QTimer * timer = new QTimer(scene);
    connect(timer, SIGNAL(timeout()), scene, SLOT(advance()));
    timer->start(1000);
    ui.myView->setScene(scene);
}

void MainWindow::populateSceneWithItems()
{
    QString filename = "D:\\shapedata.txt";
    QFile file(filename);
    if (!file.open(QIODevice::ReadOnly))
    {
        return;
    }
    QDataStream inStream(&file);
    while (!inStream.atEnd())
    {
        ShapeData shapeData;
        inStream >> shapeData;
        shapeData.addObserver(this);
        ShapeView * item = new ShapeView();
        item->setShapeData(shapeData);
        scene->addItem(item);
    }
}

void MainWindow::slotSaveItemsStates()
{
    QString filename = "D:\\shapedata.txt";
    QFile file(filename);
    if (!file.open(QIODevice::WriteOnly | QIODevice::Truncate))
    {
        return;
    }

    QDataStream outStream(&file);

    foreach(QGraphicsItem * item, scene->items())
    {
        ShapeView * shapeView = dynamic_cast<ShapeView*>(item);
        if (shapeView)
        {
            ShapeData shapeData = shapeView->getShapeData();
            outStream << shapeData;
        }
    }
    file.close();
}

void MainWindow::update(ShapeData * observedData)
{
    QString statusMessage("Noua culoare este: ");
    statusMessage += observedData->getColor().name();
    setStatusMessage(statusMessage);

    //setStatusMessage("Don't observe that anymore");
    //observedData->removeObserver(this);
}

void MainWindow::setStatusMessage(const QString & statusMessage)
{
    // update my status
    statusBar()->showMessage(statusMessage, 1000);
}

void MainWindow::setDefaultShapeData(const ShapeData & defaultShapeData)
{
    *myDefaultShapeData = defaultShapeData;
}
