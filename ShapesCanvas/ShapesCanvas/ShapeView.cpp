#include "ShapeView.h"
#include <QBrush>
#include <qpainter.h>
#include <QGraphicsScene>
#include <qdebug.h>

int ShapeView::myZValue = 0;

ShapeView::ShapeView()
    : QGraphicsItem()
    , myShapeIsPressed(false)
{
    setFlag(QGraphicsItem::ItemIsMovable);
    setFlag(QGraphicsItem::ItemIsSelectable);
}

ShapeView::~ShapeView()
{

}


QRectF ShapeView::boundingRect() const
{
    return myShapeData.getBoundingBox();
}

void ShapeView::paint(QPainter * painter,
    const QStyleOptionGraphicsItem * option,
    QWidget * widget)
{
    painter->save();

    // Draw the item boundary
    QRectF rect = boundingRect();
    QBrush brush(myShapeData.getColor());
    painter->setBrush(brush);
    painter->drawRect(rect);

    // Draw the inside circle
    double hIn = rect.height() / 4;
    double wIn = rect.width() / 4;
    QRectF rect2 = rect.adjusted(wIn, hIn, -wIn, -hIn);
    if (myShapeIsPressed)
    {
        QBrush brush(myShapeData.getSelectedColor());
        painter->setBrush(brush);
        painter->drawEllipse(rect2);
    }
    else
    {
        QBrush brush(myShapeData.getColor());
        painter->setBrush(brush);
        painter->drawEllipse(rect2);
    }
    painter->restore();
}

void ShapeView::mousePressEvent(QGraphicsSceneMouseEvent * ev)
{
    qDebug() << "ShapeView::mousePressEvent";
    myShapeIsPressed = true;
    update();
    QGraphicsItem::mousePressEvent(ev);
    setZValue(++myZValue);
}

void ShapeView::mouseReleaseEvent(QGraphicsSceneMouseEvent * ev)
{
    myShapeIsPressed = false;
    update();
    QGraphicsItem::mouseReleaseEvent(ev);
}

void ShapeView::advance(int phase)
{
    if (!phase)
    {
        return;
    }

    qDebug() << "Advance phase: " << phase;
    
    // Set a new color
    int red = qrand() % 255;
    int blue = qrand() % 255;
    int green = qrand() % 255;
    QColor color = QColor(red, blue, green);
    myShapeData.setColor(color);

    // Set a new size
    QRectF boundingBox = myShapeData.getBoundingBox();
    int xp1Diff = qrand() % 5;
    xp1Diff = qrand() % 2 ? xp1Diff : -xp1Diff;
    int yp1Diff = qrand() % 5;
    yp1Diff = qrand() % 2 ? yp1Diff : -yp1Diff;
    int xp2Diff = qrand() % 5;
    xp2Diff = qrand() % 2 ? xp2Diff : -xp2Diff;
    int yp2Diff = qrand() % 5;
    yp2Diff = qrand() % 2 ? yp2Diff : -yp2Diff;
    QRectF newBoundingBox = boundingBox.adjusted(xp1Diff, yp1Diff, xp2Diff, yp2Diff);
    prepareGeometryChange();
    myShapeData.setBoundingBox(newBoundingBox);

    // repaint the item
    update();
}
