#include "SceneViewWidget.h"

SceneViewWidget::SceneViewWidget(QWidget * parent)
    : QGraphicsView(parent)
{

}

SceneViewWidget::SceneViewWidget(QGraphicsScene * scene, QWidget * parent)
    : QGraphicsView(scene, parent)
{

}

SceneViewWidget::~SceneViewWidget()
{

}

void SceneViewWidget::paintEvent(QPaintEvent * ev)
{
    //QPainter painter(viewport());
    //QRectF aRect = rect();
    //painter.setPen(QPen(Qt::black));
    //QVector<QLine> overlayLines;
    //overlayLines << QLine(0, aRect.height()/2, aRect.width(), aRect.height()/2);
    //overlayLines << QLine(aRect.width()/2, 0, aRect.width()/2, aRect.height());
    //painter.drawLines(overlayLines);

    QGraphicsView::paintEvent(ev);
}

