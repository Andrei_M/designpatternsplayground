#pragma once
#include <qgraphicsitem.h>

class FluffyCircleItem : public QGraphicsItem
{
public:
    FluffyCircleItem();
    ~FluffyCircleItem();

    virtual QRectF boundingRect() const;
    virtual void paint(QPainter * painter,
        const QStyleOptionGraphicsItem * option,
        QWidget * widget);
};

