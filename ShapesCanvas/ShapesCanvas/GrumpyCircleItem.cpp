#include "GrumpyCircleItem.h"

#include <QPainter.h>

GrumpyCircleItem::GrumpyCircleItem()
{
}


GrumpyCircleItem::~GrumpyCircleItem()
{
}

QRectF GrumpyCircleItem::boundingRect() const
{
    return QRectF(0, 0, 200, 200);
}

void GrumpyCircleItem::paint(QPainter * painter,
    const QStyleOptionGraphicsItem * option,
    QWidget * widget)
{
    painter->setBrush(Qt::darkCyan);
    painter->drawEllipse(boundingRect());
}
