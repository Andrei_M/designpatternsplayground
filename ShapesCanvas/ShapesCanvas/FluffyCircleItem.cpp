#include "FluffyCircleItem.h"

#include <QPainter.h>

FluffyCircleItem::FluffyCircleItem()
{
}


FluffyCircleItem::~FluffyCircleItem()
{
}

QRectF FluffyCircleItem::boundingRect() const
{
    return QRectF(0, 0, 200, 200);
}

void FluffyCircleItem::paint(QPainter * painter,
    const QStyleOptionGraphicsItem * option,
    QWidget * widget)
{
    painter->setBrush(Qt::magenta);
    painter->drawEllipse(boundingRect());
}
