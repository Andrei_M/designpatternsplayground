#include "FluffySquareItem.h"

#include <QPainter.h>

FluffySquareItem::FluffySquareItem()
{
}


FluffySquareItem::~FluffySquareItem()
{
}

QRectF FluffySquareItem::boundingRect() const
{
    return QRectF(0, 0, 200, 200);
}

void FluffySquareItem::paint(QPainter * painter,
    const QStyleOptionGraphicsItem * option,
    QWidget * widget)
{
    painter->setBrush(Qt::magenta);
    painter->drawRect(boundingRect());
}
