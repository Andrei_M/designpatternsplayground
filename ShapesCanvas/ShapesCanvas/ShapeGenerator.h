
#ifndef ShapeGenerator_H
#define ShapeGenerator_H

class QGraphicsItem;

class ShapeGenerator
{
public:
    ShapeGenerator() {}
    virtual ~ShapeGenerator() {}

    virtual QGraphicsItem * makeSquare() = 0;
    virtual QGraphicsItem * makeCircle() = 0;
};
#endif // ShapeGenerator_H