#include "MainWindow.h"
#include <QtWidgets/QApplication>

#include "ShapeData.h"
#include "AnotherObserver.h"
#include <qcolor.h>

int main(int argc, char *argv[])
{
    //{
    //    ShapeData sd;
    //    Observer * ao = new AnotherObserver;
    //    sd.addObserver(ao);
    //    sd.setColor(QColor(Qt::green));
    //    delete ao;
    //}

    QApplication a(argc, argv);
    MainWindow w;
    ShapeData defaultShapeData;
    defaultShapeData.setColor(Qt::magenta);
    w.setDefaultShapeData(defaultShapeData);
    w.show();
    return a.exec();
}
