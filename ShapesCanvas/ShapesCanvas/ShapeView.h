#ifndef GRAPHICSITEM_H
#define GRAPHICSITEM_H

#include <QGraphicsItem>
#include "ShapeData.h"

class QPainter;

class ShapeView : public QGraphicsItem
{
public:
    ShapeView();
    ~ShapeView();

    ShapeData getShapeData() const { return myShapeData; }
    void setShapeData(const ShapeData & shapeData) { myShapeData = shapeData; }

    virtual QRectF boundingRect() const;
    virtual void paint(QPainter * painter,
        const QStyleOptionGraphicsItem * option,
        QWidget * widget);

protected:
    virtual void mousePressEvent(QGraphicsSceneMouseEvent * ev);
    virtual void mouseReleaseEvent(QGraphicsSceneMouseEvent * ev);
    virtual void advance(int phase);

private:
    static int myZValue;
    bool myShapeIsPressed;
    ShapeData myShapeData;
};

#endif // GRAPHICSITEM_H
